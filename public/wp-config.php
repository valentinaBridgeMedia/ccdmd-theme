<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ccdmd' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'eY6AHf/YGAS(s(NGDj^nDne*m*BY$8Ww^cr-TN@7sNiw-0(:LM`=)&%zO<Cc%-K*' );
define( 'SECURE_AUTH_KEY',  'x)igT!^CbEk&$<*Z(N17 l$wzum`/eh&uA#R:3K:mI6|gR?6qSkmT)g YK?)NU+]' );
define( 'LOGGED_IN_KEY',    'f)M1nFWvNq;oR[R>}usu4~*u<_j!n4=FASnaHs0I@$D/oULm1QrpMxPEmO%y;+;A' );
define( 'NONCE_KEY',        'v?u1<^,JCwkM(:G^aPLj+l`h!!0->UMmd2mMf~:-TE[=XLgjet/ 7VK.!G%1+/#v' );
define( 'AUTH_SALT',        '|o8p<Z4OfUoT7s4au%*D_4]f9>Gp+2wKrj^.=(i=VU+8~2B:3?IhR9;(BpK^*M#2' );
define( 'SECURE_AUTH_SALT', '^`47oU|Z~#OA(o?24&&Kk|eh]s-X~2TL&?SeP+j_t*+;*5.+oYVP~raBsfp|i=%n' );
define( 'LOGGED_IN_SALT',   'g<DbJq0Isq0Cu^shm-n5ZA>g<Q!5<+FAi]}S|$zgoXf2_n~MiB:?c6T(n8A;AT9l' );
define( 'NONCE_SALT',       'F:mir2l+wFaI&LEvYn>`L2#3~/E=/{$d=ZZW9YN|jg,k*Kk0eCx-<|^]P>;]s|M]' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bri_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
