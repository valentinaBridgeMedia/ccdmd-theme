<?php 

 /**
     * Add the theme's style scripts to the loading queue 
 */

function load_stylesheets(){

$themeVersion = wp_get_theme()->get('Version');
     
wp_register_style('themePublicStyleCss', get_template_directory_uri() . '/css/app-public.css', array(), $themeVersion, 'all');
wp_register_style('themeAdminStyleCss', get_template_directory_uri() . '/css/app-admin.css', array(), $themeVersion, 'all');

wp_register_style('themeStyleCss', get_template_directory_uri() . '/style.css', array(), $themeVersion, 'all');
wp_register_style('fontAwesomeCss', "https://use.fontawesome.com/releases/v5.7.1/css/all.css", array(), $themeVersion, 'all');

wp_enqueue_style('themePublicStyleCss');
wp_enqueue_style('themeAdminStyleCss');
wp_enqueue_style('themeStyleCss');
wp_enqueue_style('fontAwesomeCss');


 
}

add_action('wp_enqueue_scripts', 'load_stylesheets');




function js_scripts(){

    // TO DO: enqueue  Js script files
    
wp_enqueue_script('jquery');

}

add_action( 'wp_enqueue_scripts', 'js_scripts' );



function add_google_fonts() {
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;1,300;1,400&family=Ubuntu:ital,wght@0,300;0,400;1,300&display=swap', false );
    }
    
    add_action( 'wp_enqueue_scripts', 'add_google_fonts' );
?>