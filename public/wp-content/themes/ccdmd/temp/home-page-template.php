<?php

/**
 * Template Name: Modèle temp de la page d'accueil
 */

get_header();

?>
<main>
    <article class="home-page__hero background__image">
        <div class="home-page__hero__items-wrapper">
            <div class="hero__summary-wrapper ">
                <h3>
                    Développer les compétences nécessaires pour communiquer efficacement par écrit
                </h3>

            </div>
            <div class="hero__button-wrapper ">
                <a href="#" class="hero__button  link">
                    Explorer les ressources
                </a>

            </div>

        </div>

    </article>


    <section class="section__block icons">

        <!--Section Block title-->

        <div class="section__block-item">
            <h2 class="section__block-title">De la théorie à la pratique</h2>
        </div>



        <!-- Section Block icon grid -->

        <div class="section__block-item grid-3col ">

            <div class="grid__item">
                <div class="grid__item-top icon">
                    <a href="http://carceral.ccdmd.qc.ca/search?f%5B0%5D=field_filtre_2%3A824">
                        <i class="fas fa-file-alt"></i>
                    </a>
                </div>
                <div class="grid__item-bottom">
                    <p class="link">
                        <a href="http://carceral.ccdmd.qc.ca/search?f%5B0%5D=field_filtre_2%3A824">
                            <strong>Fascicules</strong>
                        </a>
                    </p>
                    <p>De la théorie sur les textes à rédiger dans le contexte de votre formation</p>

                </div>

            </div>

            <div class="grid__item">
                <div class="grid__item-top icon">
                    <a href="http://carceral.ccdmd.qc.ca/search?sort_by=weight&amp;items_per_page=50&amp;s=activit%C3%A9s-formation">

                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </div>
                <div class="grid__item-bottom">
                    <p class="link">
                        <a href="http://carceral.ccdmd.qc.ca/search?sort_by=weight&amp;items_per_page=50&amp;s=activit%C3%A9s-formation">

                            <strong>Activités</strong>

                        </a>
                    </p>
                    <p>Des activités d’écriture et de grammaire liées au contexte d’emploi</p>
                </div>

            </div>
            <div class="grid__item">
                <div class="grid__item-top icon">
                    <a href="http://carceral.ccdmd.qc.ca/search?f%5B0%5D=field_filtre_2%3A823">

                        <i class="fas fa-graduation-cap"></i>

                    </a>
                </div>
                <div class="grid__item-bottom">
                    <p class="link">
                        <a href="#">
                            <strong>Simulations<br></strong>
                        </a>
                    </p>
                    <p>Des questionnaires interactifs pour vous préparer à l’examen ministériel</p>
                </div>
            </div>

        </div>

    </section>



    <section class="section__block images no-border-botom">

        <!--Section Block title-->

        <div class="section__block-item">
            <h2 class="section__block-title">En vedette</h2>
        </div>



        <!-- Section Block icon grid -->

        <div class="section__block-item grid-3col views">

            <div class="grid__item">
                <div class="grid__item-top image type_video">
                    <a href="#">
                        <img class="" src="<?php echo get_template_directory_uri() . '/images/dev-temp/image-video1-119756.jpg'; ?>" alt=" ">
                        <span class="type_icon"> </span>
                    </a>
                </div>
                <div class="grid__item-bottom views__content link">
                    <span class="views__content-title ">
                        <a href="#" >
                            Altercation dans la cour
                        </a>
                    </span>

                </div>

            </div>

            <div class="grid__item">
                <div class="grid__item-top image type_resource">
                    <a href="#">
                        <img class="" src="<?php echo get_template_directory_uri() . '/images/dev-temp/vignette_objet1.png'; ?>" alt=" ">
                        <span class="type_icon"> </span>
                    </a>
                </div>
                <div class="grid__item-bottom views__content link" >
                    <span class="views__content-title">
                        <a href="#">
                            Objet disparu
                        </a>
                    </span>

                </div>

            </div>

            <div class="grid__item">
                <div class="grid__item-top image type_resource">
                    <a href="#">
                        <img class="" src="<?php echo get_template_directory_uri() . '/images/dev-temp/vignette_objet2.png'; ?>" alt=" ">
                        <span class="type_icon"> </span>
                    </a>
                </div>
                <div class="grid__item-bottom views__content link">
                    <span class="views__content-title">
                        <a href="#">
                            Simulation d’examen C
                        </a>
                    </span>

                </div>

            </div>


        </div>

        </div>

    </section>



    <section class="section__color-block triangle-decor">

        <div class="section__color-block-wrapper ">

            <h3>Améliorez vos compétences en français<br class="minify"> pour mieux intervenir en milieu correctionnel
            </h3>

            <div class="empty-button-wrapper">
                <a class="empty-button link" href="#">Explorer les ressources</a>
            </div>
        </div>

    </section>

</main>




<?php get_footer(); ?>