<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CCDMD</title>

    <?php wp_head(); ?>

</head>

<body <?php body_class(is_404() ? 'page' : ''); ?>>
    <header class="bootstrap-wrapper">
        <div class="col-12 header__top-bar">

            <a href="../../ccdmd/public/" class="top-bar__home-icon">

                <img src="<?php echo get_template_directory_uri() . '/images/maison.png'; ?>" alt="Accueil">
            </a>

        </div>
        <div class="header__branding">

            <!-- start: Branding -->
            <div class="branding__elements">



                <!-- start: Site name and Slogan -->
                <section class="branding__site-name-and-slogan">

                    <h1 class="site-name">
                        <a href="../../ccdmd/public/" title="Page d'accueil">Français en milieu&nbsp;carcéral</a>
                    </h1>

                    <h2 class="site-slogan">Des ressources pour les futurs agents&nbsp;correctionnels</h2>



                </section><!-- /end Site name and slogan -->

                <div class="branding__logo">

                    <a href="http://www.ccdmd.qc.ca/" title="Logo">
                        <img class="site-logo" src="<?php echo get_template_directory_uri() . '/images/logo.png'; ?>" alt="Logo du site CCDMD">
                    </a>

                </div>


            </div>
            <!-- /end Branding -->
        </div>
    </header>

    <!-- --start:Site main navigation -->
    <div class="primary-navigation__bar">
        <nav class="primary-navigation__menu menu-wrapper">
            <ul class="menu primary-menu ">
                <li class="primary-menu-item active">
                    <a href="../../ccdmd/public/"  >
                        <span>Accueil</span>
                    </a>
                </li>
                <li class="primary-menu-item">
                    <a href="#" >
                        <span>Ressources</span>
                    </a>
                </li>
                <li class="primary-menu-item">
                    <a href="#">
                        <span>À propos</span>
                    </a>
                </li>
            </ul>
        </nav>
     </div> <!--/end Site main navigation -->