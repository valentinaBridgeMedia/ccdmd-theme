<?php wp_footer(); ?>

<footer class="footer">
    <div class="footer__wrapper">

        <div class="footer__wrapper-top">
            <section class="footer__block-menu">
                <h2 class="footer__block-menu-title">Menu</h2>
                <div class="footer__block-menu-content">
                    <ul class="footer__menu ">
                        <li class="footer__menu-item link">
                            <a href="#">
                                <span>Accueil</span>
                            </a>
                        </li>
                        <li class="footer__menu-item link">
                            <a href="#" title="">
                                <span>Ressources</span>
                            </a>
                        </li>
                        <li class="footer__menu-item link">
                            <a href="#" title="">
                                <span>À propos</span>
                            </a>
                        </li>
                        <li class="footer__menu-item link">
                            <a href="#" title="">
                                <span>Droits d’utilisation</span>
                            </a>
                        </li>
                        <li class="footer__menu-item link">
                            <a href="#" title="">
                                <span>Générique du site</span>
                            </a>
                        </li>
                        <li class="footer__menu-item link">
                            <a href="#" title="">
                                <span>Commentaires</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
            <div class=" block__social-media no-title ">

                    <!-- Social media icons BEGIN -->
                    <div class="block__social-media-wrapper">
                        <div class="social-media__icons">
                            <a class="social-media__icon-more" target="_blank" title="More" href="#">
                                <img src="<?php echo get_template_directory_uri() . '/images/dev-temp/social-media/reseaux-sociaux-plus.png' ?>" alt="Plus...More...">
                            </a>
                            <a class="social-media__icon-facebook" title="Facebook" href="#">
                                <img src="<?php echo get_template_directory_uri() . '/images/dev-temp/social-media/reseaux-sociaux-facebook.png' ?>" alt="Partager sur Facebook">
                            </a>
                            <a class="social-media__icon-twitter" title="Twitter" href="#">
                                <img src="<?php echo get_template_directory_uri() . '/images/dev-temp/social-media/reseaux-sociaux-twitter.png' ?>" alt="Partager sur Twitter">
                            </a>
                            <a class="social-media__icon-rss" href="/rss" title="Fil RSS">
                                <img src="<?php echo get_template_directory_uri() . '/images/dev-temp/social-media/reseaux-sociaux-rss.png' ?>" alt="Fil RSS">
                            </a>
                        </div>
                    </div>
                    <!-- Social media icons END -->
            </div>
        </div>


        <div class="footer__wrapper-bottom">
            
                <div class="footer_copyright text-align-right right">

                    <p>© Centre collégial de développement de matériel didactique, Collège de Maisonneuve, 2020</p>

                </div>
            
        </div>

    </div>
</footer>

</body>

</html>